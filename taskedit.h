#ifndef TASKEDIT_H
#define TASKEDIT_H

#include <QDialog>
#include "task.h"
#include "taskmanager.h"
#include "kanbanwindow.h"

namespace Ui {
class TaskEdit;
}

class TaskEdit : public QDialog
{
    Q_OBJECT

public:
    explicit TaskEdit(QWidget *parent = 0, KanbanWindow* drawer = 0, TaskManager *taskManager = 0);
    ~TaskEdit();
    void setTask(Task *task);

signals:
    void saveSignal(Task *task, bool isNew);
//    void deleteSignal(Task *task);

private slots:
    void on_pushButtonOk_clicked();

    void on_pushButtonCancel_clicked();

    void on_pushButtonDelete_clicked();

    void on_qLineEditName_textEdited(const QString &arg1);

    void on_qLineEditAssigned_textEdited(const QString &arg1);

    void on_qLineEditDescription_textEdited(const QString &arg1);

    void on_comboBoxStatus_currentIndexChanged(int index);

private:
    Ui::TaskEdit *ui;
    Task* currentTask = NULL;
    void detectChange();
    void save();
    void remove();
    TaskManager *taskManager;
};

#endif // TASKEDIT_H
