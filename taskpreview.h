#ifndef TASKPREVIEW_H
#define TASKPREVIEW_H

#include <QWidget>
#include <QString>
#include "task.h"
#include "taskmanager.h"

namespace Ui {
class TaskPreview;
}

class TaskPreview : public QWidget
{
    Q_OBJECT

public:
    explicit TaskPreview(QWidget *parent = 0, TaskManager *taskManager = 0);
    ~TaskPreview();
    void setTask(Task *task);
    Task* task();

//signals:
//    void redraw();

//public slots:
//    void onTaskChanged(Task *task);
//    void onTaskDeleted(Task *task);

private slots:
    void on_pushButtonDetails_clicked();

private:
    Ui::TaskPreview *ui;
    Task *currentTask;
    TaskManager *taskManager;
};

#endif // TASKPREVIEW_H
