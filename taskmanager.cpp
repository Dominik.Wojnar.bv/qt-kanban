#include "taskmanager.h"
#include <QUuid>
#include <cstdlib>

TaskManager::TaskManager()
{
    statusNames = new QStringList();
    statusNames->append("Backlog");
    statusNames->append("To do");
    statusNames->append("In progress");
    statusNames->append("Under review");
    statusNames->append("Done");
    load();
}

TaskManager::~TaskManager()
{
//    delete statusNames;
//    delete tasks;
}

QStringList* TaskManager::getStatusNames(){
    return statusNames;
}

void TaskManager::load(){
    tasks = new QVector<QVector<Task*>*>();
    for(int statusId =0; statusId<statusNames->size(); statusId++) {
        QVector<Task*>* column = new QVector<Task*>();
        int randomColumnSize = rand()%13;        // TODO REMOVE
        for(int j=0;j<randomColumnSize; j++){    // TODO REMOVE
            Task* t = TaskManager::getAnyTask(); // TODO REMOVE
            t->setState(statusId);               // TODO REMOVE
            column->append(t);
        }
        tasks->append(column);
    }
//    onChange();
}

void TaskManager::remove(Task* task){
    for(int statusId=0; statusId<statusNames->size(); statusId++) {
        QVector<Task*>* column = tasks->at(statusId);
        int columnSize = column->size();
        for(int taskId=columnSize-1; taskId>0; taskId--){
            if(column->at(taskId)->id() == task->id()){
                column->removeAt(taskId);
                return;
            }
        }
    }
    onChange();
}

void TaskManager::onChange(){
    for(int statusId=0; statusId<statusNames->size(); statusId++) {
        QVector<Task*>* column = tasks->at(statusId);
        int columnSize = column->size();
        for(int taskId= columnSize - 1; taskId >= 0; taskId--){
            Task* task = column->at(taskId);
            int currentState = task->state();
            if(currentState != statusId){
                tasks->at(currentState)->append(task);
                column->removeAt(taskId);
            }
        }
    }
}

QString TaskManager::getStatusName(int statusId) {
    if(statusNames->size() > statusId)
        return statusNames->at(statusId);
    else
        return QString::number(statusId);
}

QVector<Task*>* TaskManager::getTasksByStatus(int statusId)
{
    return tasks->at(statusId);
}


Task* TaskManager::getAnyTask(){
    QUuid uuid = QUuid::createUuid();
    quint32 state = rand() % 5 ;

    return new Task(
                uuid.toString(),
                state,
                QString("Ala ma kota"),
                QString("Mateusz Siano"),
                QString("Dać kotu jeść"));
}

