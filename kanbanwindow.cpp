#include "kanbanwindow.h"
#include "taskedit.h"

KanbanWindow::KanbanWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::KanbanWindow)
{
    ui->setupUi(this);
    taskManager = new TaskManager();
    draw();
}


KanbanWindow::~KanbanWindow()
{
    delete ui;
}

void KanbanWindow::draw(){
    QScrollArea* horizontalScrollArea = new QScrollArea();
    horizontalScrollArea->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
    horizontalScrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    horizontalScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);

    QWidget* area = new QWidget(horizontalScrollArea);
    QHBoxLayout* layout = new QHBoxLayout();
    area->setObjectName(QString("area"));
    for (int i = 0; i < 5; i++)
    {
        addColumn(layout, i);
    }
    area->setLayout(layout);
    horizontalScrollArea->setWidget(area);
    ui->verticalLayout->addWidget(area, 0);
}

void KanbanWindow::changeTask(Task* task, bool isNew){
    QVBoxLayout* layout = getColumn(task->state());
    if(isNew){
        addTask(layout, task);
    } else {
        layout->findChild<TaskPreview*>(QString("TaskPreview_") + task->id())->setTask(task);
    }
}

QVBoxLayout* KanbanWindow::getColumn(int statusId){
    QString columnName = QString("Column_") + QString::number(statusId);
    return  ui->
            verticalLayoutWidget->
            findChild<QWidget*>("area")->
            findChild<QScrollArea*>(columnName)->
            findChild<QWidget*>("qt_scrollarea_viewport")->
            findChild<QWidget*>("columnArea")->
            findChild<QVBoxLayout*>("layout");
}

void KanbanWindow::addColumn(QHBoxLayout* horizontalScrollAreaLayout, int statusId){
    QVector<Task*>* tasks = taskManager->getTasksByStatus(statusId);

    QScrollArea* verticalScrollArea = new QScrollArea();
    QString columnName = QString("Column_") + QString::number(statusId);
    verticalScrollArea->setObjectName(columnName);
    verticalScrollArea->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    verticalScrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    verticalScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    horizontalScrollAreaLayout->addWidget(verticalScrollArea, statusId);
    QWidget *area = new QWidget(verticalScrollArea);
    area->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);

    area->setObjectName(QString("columnArea"));
    QVBoxLayout* layout = new QVBoxLayout(area);
    layout->setObjectName(QString("layout"));
    area->setLayout(layout);
    QLabel *statusNameLabel = new QLabel();

    QString statusName = taskManager->getStatusName(statusId);
    statusNameLabel->setText(statusName);
    layout->addWidget(statusNameLabel, 0);
    for (int i = 0; i < tasks->size(); i++)
    {
        addTask(layout, tasks->at(i), i);
    }
    verticalScrollArea->setWidget(area);
}

void KanbanWindow::addTask(QVBoxLayout* layout, Task* task, int position){
    TaskPreview* panelTaskPreview = new TaskPreview(this, taskManager);
    panelTaskPreview->setTask(task);
    panelTaskPreview->setObjectName(QString("TaskPreview_") + task->id());
    panelTaskPreview->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    layout->addWidget(panelTaskPreview, position);
}

void KanbanWindow::on_pushButton_clicked()
{
    TaskEdit *editor = new TaskEdit(this, (KanbanWindow*) this, taskManager);
    Task* task = TaskManager::getAnyTask();
    editor->setTask(task);
    editor->show();
//    changeTask(task, true);
}
