# QtKanban

This is an excerpt from the program I wrote when I learned the QT framework.

## What is kanban?

Kanban is a scheduling system.

It looks like an array(billboard) with different columns. Each column represents the status of the tasks it contains. The columns contain pages with the task description and the contractor's assignment.

Kanban is one method to achieve JIT.

## Dependencies

qt5.9.5 (or newer)

## Known bugs

- saving changes is not possible
- new tasks overlap existing ones
