#include "taskedit.h"
#include "ui_taskedit.h"
#include "taskmanager.h"
TaskEdit::TaskEdit(QWidget *parent, KanbanWindow* drawer, TaskManager* taskManager) :
    QDialog(parent),
    ui(new Ui::TaskEdit)
{
    this->taskManager = taskManager;
    ui->setupUi(this);
    ui->comboBoxStatus->addItems(*(taskManager->getStatusNames()));
    connect (this, &TaskEdit::saveSignal, drawer, &KanbanWindow::changeTask);
//    connect (this, SIGNAL (saveSignal(Task*, bool)), drawer, SLOT(changeTask(Task*, bool)));
}

TaskEdit::~TaskEdit()
{
    delete ui;
}

void TaskEdit::setTask(Task* task)
{
    currentTask = task;
    QString name = task->name();
    ui->qLineEditName->setText(name);
    setWindowTitle(name);
    ui->qLineEditAssigned->setText(task->assigned());
    ui->qLineEditDescription->setText(task->description());
    ui->comboBoxStatus->setCurrentIndex(task->state());

    ui->pushButtonDelete->hide();
}

void TaskEdit::on_pushButtonOk_clicked()
{
    save();
    close();
}

void TaskEdit::on_pushButtonCancel_clicked()
{
    close();
}

void TaskEdit::on_pushButtonDelete_clicked()
{
    remove();
    close();
}

void TaskEdit::save()
{
    currentTask->setName(ui->qLineEditName->text());
    currentTask->setAssigned(ui->qLineEditAssigned->text());
    currentTask->setDescription(ui->qLineEditDescription->text());
    currentTask->setState(ui->comboBoxStatus->currentIndex());
    setWindowTitle(ui->qLineEditName->text());
    taskManager->onChange();
    emit saveSignal(currentTask, false);
}

void TaskEdit::remove()
{
//    emit deleteSignal(currentTask);
}

void TaskEdit::detectChange()
{
    setWindowTitle(ui->qLineEditName->text() + "*");
}

void TaskEdit::on_qLineEditName_textEdited(const QString &arg1)
{
    detectChange();
}

void TaskEdit::on_qLineEditAssigned_textEdited(const QString &arg1)
{
    detectChange();
}

void TaskEdit::on_qLineEditDescription_textEdited(const QString &arg1)
{
    detectChange();
}

void TaskEdit::on_comboBoxStatus_currentIndexChanged(int index)
{
    if(currentTask != NULL
         && index != currentTask->state() ) {
        detectChange();
    }
}
