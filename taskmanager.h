#ifndef TASKMANAGER_H
#define TASKMANAGER_H
#include <QStringList>
#include <QVector>
#include <QObject>
#include "task.h"

class TaskManager
{
public:
    TaskManager();
    ~TaskManager();
    QString getStatusName(int statusId);
    QStringList *getStatusNames();

    QVector<Task*>* getTasksByStatus(int statusId);
    static Task* getAnyTask();
    void onChange();
    void remove(Task* task);

private:
    QStringList *statusNames;
    void load();
    QVector<QVector<Task*>*>* tasks;
};

#endif // TASKMANAGER_H
