#include "task.h"

Task::Task()
{

}

Task::Task( const QString &id, const int state, const QString &name, const QString &assigned, const QString &description)
{
    _id = id;
    _state = state;
    _name = name;
    _assigned = assigned;
    _description = description;
}

QString Task::id(){
    return _id;
}

QString Task::name()
{
    return _name;
}

void Task::setName(const QString &name)
{
    _name = name;
}

void Task::setAssigned(const QString &assigned)
{
    _assigned = assigned;
}

void Task::setDescription(const QString &description)
{
    _description = description;
}

QString Task::description()
{
    return _description;
}

QString Task::assigned()
{
    return _assigned;
}

int Task::state()
{
    return _state;
}

void Task::setState(const int state)
{
    _state = state;
}

void Task::read(const QJsonObject &json)
{
    if (json.contains("name") && json["name"].isString())
        _name = json["name"].toString();

    if (json.contains("assigned") && json["assigned"].isString())
        _assigned = json["assigned"].toString();

    if (json.contains("description") && json["description"].isString())
        _description = json["description"].toString();

    if (json.contains("id") && json["id"].isString())
        _id = json["id"].toString();
}

void Task::write(QJsonObject &json) const
{
    json["id"] = _id;
    json["name"] = _name;
    json["assigned"] = _assigned;
    json["description"] = _description;
}
