#ifndef KANBANWINDOW_H
#define KANBANWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QScrollArea>
#include <QLabel>
#include <QVector>
#include <QPushButton>

#include "ui_kanbanwindow.h"
#include "taskpreview.h"
#include "taskmanager.h"
#include "task.h"

namespace Ui {
class KanbanWindow;
}

class KanbanWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit KanbanWindow(QWidget *parent = 0);
    ~KanbanWindow();
    QVBoxLayout* getColumn(int statusId);

public slots:
//    void redraw();
    void changeTask(Task* task, bool isNew);

private slots:
    void on_pushButton_clicked();

private:
    Ui::KanbanWindow *ui;
    void addTask(QVBoxLayout* layout, Task* task, int position = 0);
    void addColumn(QHBoxLayout* verticalScrollAreaLayout, int position);
    void draw();

    TaskManager* taskManager;
};

#endif // KANBANWINDOW_H
